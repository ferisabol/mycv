# mycv

Task I.
----------
Develop containerized app running web server and serving your Online CV.
Let's call such app myCV.
Bundle your web page code into Docker Image and store it into Docker HUB.


Steps:
------
1.  deploy aCloudGuru Playground VM as follows:
    
    hostname: builder<br />
    OS: centos7<br />
    VM_size: micro<br />

2.  add extra HDD of capacity ~1-2 GB and attach it to your VM

3.  from an extra HDD create create:
    
    VG: 		data / 1GB<br />
    LV: 		docker / 500MB<br />
    LV filesystem: 	ext4<br />
    mountpoint for LV: 	/var/lib/docker<br />
    
4.  ensure that Docker Engine is installed and running, must survive reboot

5.  create build directory /build which will hold your code

6.  develop index.html containing your real data
    
    Use the following code as example:<br />
    https://github.com/ritaly/HTML-CSS-CV-demo

7.  store your code into GitLab, name your project **"mycv"**

8.  create Docker Image containing your code + web server

    Name of Image: myCV:final<br />
    Use: nginx:stable as Base for your Image

9.  use your account within Docker Hub and push your Image in to it

10. test your doings by creating Docker Container from your Image on your VM

    Name of container: my-cv<br />
    Expose port: 80

11. Create local user instructor within group devops
    
    user: instructor<br />
    uid: 666<br />
    password: Passw0rd<br />
<br />
    group: devops<br />
    gid: 666<br />

solution (11):
***************************
getent group | grep devops<br />
groupadd -g 666 devops<br />
useradd instructor -u 666 -g 666 -m -s /bin/bash<br />
passwd instructor<br />
***************************


Task II.
----------
Implement GitLab CI/CD Pipeline.

Steps:
------
1. develop a simple GitLab CI/CD Pipeline which will automate the process of building and pushing your Image into your Docker HUB registry in case that content of index.html has changed.
    stages:<br />
    - build<br />
<br />   
    jobs:<br />
    build_job<br />



Task III.
----------
Deploy real K8S cluster using kubeadm.


Steps:
------
1. deploy 3 VMs using aCloudGuru playground
   OS: Ubuntu 20.04 LTS<br />
   Image: 2xCPU + 4GB RAM<br />
   Hostnames:<br />
    - master1<br />
    - worker1<br />
    - worker3<br />
   
2. perform OS update on all 3 machines

3. Create local user instructor within group devops on all 3 VMs

    user: instructor<br />
    uid: 666<br />
    password: Passw0rd<br />
<br />
    group: devops<br />
    gid: 666<br />

3. Install K8S (1x master + 2x worker nodes)<br />
   Follow the official docu for detailed installation steps for a particular distro: 
   
     https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/

4. Deploy your first APP<br />

   > create deployment named mycv with 3 replicas from Image you have stored in DockerHub<br />
   > expose your deployment as service named mycv-svc, ports=80:80, type=NodePort

5. on master1 install & start httpd service, store your email address into \

   /var/www/html/index.html

Check:<br />
- please provide your Public IP build machine & master1
